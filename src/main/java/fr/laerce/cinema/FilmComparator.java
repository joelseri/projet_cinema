package fr.laerce.cinema;

import java.util.Comparator;

public class FilmComparator implements Comparator<Film> {

    public int compare(Film o1, Film o2) {
        return o1.afficheNom.compareToIgnoreCase(o2.titre);
    }
}
