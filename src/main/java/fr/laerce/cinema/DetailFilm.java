package fr.laerce.cinema;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by fred on 03/02/2016.
 */
public class DetailFilm extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {


        Integer id = Integer.parseInt(request.getPathInfo().substring(1));
        FilmsDonnees fd = new FilmsDonnees();
        Film film = fd.getById(id);

        HttpSession session = request.getSession(true);

        // pour les besoins de la vue
        request.setAttribute("film", film);


        //request.getSession(true);
//        System.out.println(session.getId());
//        System.out.println(session.getAttribute("nom"));

        List<Film> filmConsulter;
        if (session.getAttribute("filmConsulter") != null) {
            filmConsulter = (ArrayList) session.getAttribute("filmConsulter");
            filmConsulter.add(film);


        } else {
            filmConsulter = new ArrayList<>();
            filmConsulter.add(film);


        }
        session.setAttribute("filmConsulter", filmConsulter);

//

        // délégation à la vue
        String jspview = "DetailFilm.jsp";
        getServletConfig().getServletContext()
                .getRequestDispatcher("/WEB-INF/jsp/" + jspview).forward(request, response);


    }
}
