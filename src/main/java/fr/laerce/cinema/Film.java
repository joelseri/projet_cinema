package fr.laerce.cinema;

public class Film {
    public Film(int id, String titre, String afficheNom, double note) {
        this.id = id;
        this.titre = titre;
        this.afficheNom = afficheNom;
        this.note = note;
    }

    int id;
    String titre;
    String afficheNom;
    double note;


    public String toString() {
        return super.toString();
    }

    public int getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public String getAfficheNom() {
        return afficheNom;
    }

    public double getNote() {
        return note;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setAfficheNom(String afficheNom) {
        this.afficheNom = afficheNom;
    }

    public void setNote(double note) {
        this.note = note;
    }
}