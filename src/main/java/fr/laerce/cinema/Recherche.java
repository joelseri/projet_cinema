package fr.laerce.cinema;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by fred on 03/02/2016.
 */
public class Recherche extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        //je recupere ma valeur renseigné dans le champs saisi
        //PrintWriter out = response.getWriter();
        String recherche = request.getParameter("recherche");


        FilmsDonnees fd = new FilmsDonnees();
        List<Film> FilmConsulter = new ArrayList<>();
        request.setAttribute ("films", FilmConsulter);

        for (Film film : fd.lesFilms) {
            if (recherche != "" && (film.titre.toLowerCase().contains(recherche.toLowerCase()))) {
//                out.println("<a href='detailFilm/" + film.id + "'>"
//                        + film.titre + " (" + film.note + ") </a>");
//                out.println("  </li>");
//                out.println("<img style='margin-left:20px;height:auto;width:100px' src='affiche/" + film.id + "'/>");
                FilmConsulter.add(film);

            }

        }



        String jspview = "Recherche.jsp";
        getServletConfig().getServletContext()
                .getRequestDispatcher("/WEB-INF/jsp/"+jspview).forward(request, response);

    }



    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {



    }
}
