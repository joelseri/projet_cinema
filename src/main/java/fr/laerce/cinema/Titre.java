package fr.laerce.cinema;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by fred on 03/02/2016.
 */
public class Titre extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        //je recupere ma valeur renseigné dans le champs saisi


    }

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {

        FilmsDonnees fd = new FilmsDonnees();
        // sort ?
        String sort = request.getParameter("sort");
        if (sort == null) {
            Collections.sort(fd.lesFilms, new FilmComparator());
        } else if ("note".equals(sort)) {
            Collections.sort(fd.lesFilms, new Comparator<Film>() {
                @Override
                public int compare(Film o1, Film o2) {
                    return Double.compare(o1.note, o2.note);
                }
            });
        } else if ("notedesc".equals(sort)) {
            Collections.sort(fd.lesFilms, (Film o1, Film o2) -> Double.compare(o2.note, o1.note));
        }


        // boucler sur les films
        // pour les besoins de la vue
        request.setAttribute ("films", fd.lesFilms);
        String jspview = "Titre.jsp";
        getServletConfig().getServletContext()
                .getRequestDispatcher("/WEB-INF/jsp/"+jspview).forward(request, response);


    }
}
