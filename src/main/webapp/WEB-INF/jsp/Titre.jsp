<%--
  Created by IntelliJ IDEA.
  User: CDI
  Date: 14/11/2018
  Time: 09:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Liste de films</title>
</head>
<body>

    <c:forEach items="${films}" var="film">

            <img src="affiche/${film.id}" >
          <figcaption> <a href="DetailFilm/${film.id}">${film.titre}</a>
            &ensp;Note: ${film.note}</figcaption>


    </c:forEach>
</body>
</html>
